#!/bin/bash

echo "Wyswietla od 8 do 12 wierszu"
awk ' NR==8,NR==12 ' /etc/passwd

echo --------------------------------------

echo "Zmienia separator pul na : z spacji"
awk -F ":" '{ print $1 }' /etc/passwd

echo --------------------------------------

echo "Nr liczby wierszy w pliku /etc/passwd"
awk -F ":" '{ print $1 } END { print NR }' /etc/passwd

echo --------------------------------------
###Dodanie formatowania tekstu
awk -F ":" '{ print $1 } END { print "LICZBA WIERSZY:",NR } ' /etc/passwd


echo --------------------------------------
echo "Wyswietla tylko wartość wiekszą niz 50 z pliku awk-test" 
awk '{if ($1 > 50) print $1}' awk-test

echo --------------------------------------
echo "Złozony skrypt awk"
awk '{
if ($1 > 50)
{
x = $1 * 2
print x
} else
177
{
x = $1 * 3
print x
}}' awk-test