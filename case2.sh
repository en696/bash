#!/bin/bash
usage="Składnia: search.sh plik ciąg_znaków operacja"
if [ ! $# -eq 3 ] ; then
echo "$usage"
exit 2
fi
[ ! -f $1 ] && exit 3
case $3 in
[cC])
mesg="Zliczanie ilości wystąpień ciągu znaków $2 w pliku $1"
opt="-c"
;;
[pP])
mesg="Wyświetlanie wszystkich wystąpień ciągu znaków $2 w pliku $1"
opt=""
;;
[dD])
mesg="Wyświetlanie zawartości pliku $1 z wyjątkiem wierszy zawierających
ciąg znaków $3"
opt="-v"
;;
*) echo "Niepoprawne parametry wywołania $1 $2 $3";;
esac
echo $mesg
grep $opt $2 $1