#!/bin/zsh

if [[ $# -eq 0 ]]; then
echo """Bład !!!!!!!!!!!!!!!!!!!
Podaj jeden argument.
Jako argument podaj sciezke do pliku z logami nginxa które mają zostać podane analizie
"""
exit 1
fi


if [[ $# -gt 1 ]]; then
echo """Bład !!!!!!!!!!!!!!!!!!!
Podaj tylko jeden argument.
Podano argumentów $#.
Jako argument podaj sciezke do pliku z logami nginxa które mają zostać podane analizie
"""
exit 1
fi

if [ ! -f $1 ]; then 
    echo """Bład !!!!!!!!!!!!!!!!!!!
Plik $1 nie istnieje lub uzytkownik $USER  niema do niego dostepu lub nie jest plikiem tekstowym
    """
exit 1
fi

ERROR_IP=`awk '{if($9 > 399)
    {print $1};
}' $1 | sort | uniq -c | sort -r | head -n 25`

COD_ERROR=`awk '{if($9 > 399) 
    {print $9};
    }' $1 | sort | uniq -c | sort -r`

echo "Adresy IP który generuje najwieciej błedów: 
$ERROR_IP"

MOST_OFTEN=`echo $COD_ERROR | awk  'NR==1{ print $2}'`
COD_COUNT=`echo $COD_ERROR | awk  'NR==1{ print $1}'`


TIME_START_LOG_DAY=`head -n1 $1 | awk '{ print $4}' | tr -d [ | tr ":" " "| awk '{print $1}' | tr "/" "-"`
TIME_START_LOG_HOURS=`head -n1 nginx_logs  | awk '{ print $4}' | tr -d [ | tr ":" " " | awk '{print  $2,$3,$4}' | tr " " ":"`



TIME_END_LOG_DAY=`tail -n1 $1 | awk '{ print $4}' | tr -d [ | tr ":" " "| awk '{print $1}' | tr "/" "-"`
TIME_END_LOG_HOURS=`head -n1 nginx_logs  | awk '{ print $4}' | tr -d [ | tr ":" " " | awk '{print  $2,$3,$4}' | tr " " ":"`


echo "Plik $1 z logami nginixa rozpoczyna się dnia $TIME_START_LOG_DAY o godzinie $TIME_START_LOG_HOURS a konczy dnia  $TIME_END_LOG_DAY o godzinie $TIME_END_LOG_HOURS"

echo "-----------------------------------------------------------"

echo "Najczejsciej zwracanym błedem jest $MOST_OFTEN i wystopił $COD_COUNT"

echo "-----------------------------------------------------------"
echo """Ilość wystąpień błedy w pliku:
$COD_ERROR
"""


echo "Przegladarki internetowe"
awk '{ browser[$12]++ }
END {
for ( b in browser )
print b, " został(a) użyta ", browser[b], " raz(y)."
}' $1

