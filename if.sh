#!/bin/bash


przyklad_1="/home/zbigniew/.zsh_history"

if [ -f $przyklad_1 ]; then
    echo $przyklad_1 jest zwykłym plikiem
fi

przyklad_2="/home/zbigniew"
file2=`file $przyklad_2 | cut -d ":" -f 2`

if [ -f $przyklad_2 ]; then
    echo $przyklad_2 jest zwykłym plikiem
    else
    echo $przyklad_2 nie jest zwykłym plikiem a jest $file2
fi

przyklad_3="/home/zbigniew"
file3=`file $przyklad_3 | cut -d ":" -f 2`

if [ -d $przyklad_3 ]; then
    echo $przyklad_3 jest zwykłym katalogiem
    else
    echo $przyklad_3 nie jest katalogiem a jest $file3
fi


przyklad_4="/boot/vmlinuz"

if [ -h $przyklad_4 ]; then
    echo $przyklad_4 jest symlinkiem
    else
    echo $przyklad_4 nie jest symlinkiem
fi



przyklad_5="/home/zbignieww"

if [ -h $przyklad_5 ]; then
    echo $przyklad_5 jest symlinkiem
    elif [ -f $przyklad_5 ]; then 
    echo $przyklad_5 nie jest zwykłym plikiem
    elif [ -d  $przyklad_5 ]; then
    echo $przyklad_5 jest zwykłym katalogiem
    elif [ ! -e $przyklad_5 ]; then
    echo $przyklad_5 nie istnieje !!!!!!!
    else
    echo Blad nie znany rodzaj pliku  !!!!!!
fi



przyklad6="/boot/vmlinuz"
a="10"
b="5"

if [ $b -eq "2" ] && [ $b -eq "4" ] || [ -h $przyklad_6 ] ; then
    echo Warunki są prawdziwe 
else
    echo warunki są nie prawdziwe 
fi



if [ $# -eq 1 ]; then
    echo "Podano argument: $1" 
else
    echo "Podaj jeden argument sciezke do pliku"
    exit 1
fi

if [ -e $1 ]; then 
    echo "Sciezka $1 jest prawidłowa"
else
    echo "Sciezka $1 nie istnieje"
fi


if [ -x $1 ]; then 
    echo "Sciezka $1 prowadzi do pliku który ma uprawnienia do wykonywania"
else
    echo "Sciezka $1  prowadzi do pliku bez uprawnien wykonywania"
fi

if [ -e $1 ]; then
    echo "plik $1 istnieje"
    if [ -r $1 ]; then
        echo "Mozna odczytać plik $1"
    else
        echo "Nie mozna odczytać pliku $1"
    fi
else
    echo "Blad plik $1 nie istnieje"
fi


if [ -O $1 ]; then
echo "Plik nalezy do osoby wykonujacej ten skrypt: $USER"
else
echo "Plik nie nalezy do $USER"
fi

