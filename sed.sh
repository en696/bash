#!/bin/bash

file="test-file-dla-sed"

echo "Wstawia testk przed 2 wieszu pliku $file"
sed '2i\wstawiony tekst' $file

echo -e  "\n---------------------------"

echo "Wstawia testk po 2 wieszu pliku $file"
sed '2a\wstawiony tekst' $file

echo -e  "\n---------------------------"

echo -e "Kasuje testk z 2 wieszu pliku $file"
sed '2d' $file

echo -e  "\n---------------------------"

echo "podmienia testk w 2 wieszu pliku $file"
sed '2c\zmodyfikowany drugi wiersz tekstu' $file

echo -e  "\n---------------------------"

sed -E 'y/[a-z]*/[A-Z]*/' $file