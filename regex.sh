#!/bin/bash

echo "Wyswietl wszystkie nie puste linie "
awk '!/^$/{print $0}' /etc/group

echo "Wyswietl wszystkie linie gdzie wysteuje ash ale które nie rozpozynają się od k ani n"
awk '/[^kn]ash/{print $0}' /etc/passwd


echo "Wyswietl wszystkie linie gdzie wystepuje słowo rozpoczynajace się od małego t i zaweirajace w srodku słowa jedna z liter a e o r i konczacza się na małe t"
echo "toot" | awk '/t[aeor]*t/{print $0}'
echo "tent" | awk '/t[aeor]*t/{print $0}'
echo "tart" | awk '/t[aeor]*t/{print $0}'




echo "Dopasowuje jeden z podanych wzorow po |"
echo "welcome bash scripting" | awk '/Linux|bash|shell/{print $0}'
echo "welcome shell scripting" | awk '/Linux|bash|shell/{print $0}'


echo "Dopasowuje cały ciag znajdujacy się w nawiasach ()"
echo "welcome to shell scripting" | awk '/(shell scripting)/{print $0}'