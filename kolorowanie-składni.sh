#!/bin/bash

if [ $1 -gt $2 ]; then
echo -e "\033[32margument $1 jest wiekszy niz argument $2\033[32m"
elif [ $1 -eq $2 ]; then
echo -e "\033[34mobie liczby są rowne\033[34m"
else
echo -e "\033[31margument $1 jest mniejszy niz argument $2\033[31m"
fi