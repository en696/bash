#!/bin/bash
# Skrypt pozwalający na ocenę wyników (skala od A do F)
# Skłania: grade.sh student wynik
# Autor: @likegeeks
# Data ostatniej modyfikacji: 1971-01-01
if [ ! $# -eq 2 ] ; then
echo "Składnia: grade.sh <student> <wynik [A-F]>"
exit 2
fi
case ${2^^} in #Rozwijanie parametrów zostało użyte do konwersji wprowadzanych znaków na wielkie litery
[A-C]) echo "$1 - znakomity wynik"
;;
[D]) echo "$1 - mogło być lepiej!"
;;
[E-F]) echo "$1 - trzeba będzie jeszcze trochę nad tym popracować"
;;
*) echo "Nie można ocenić wyników dla $1 $2"
;;
esac