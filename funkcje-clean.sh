#!/bin/bash
# Skrypt prosi o podanie nazwy pliku,
# a następnie usuwa z niego zakomentowane i puste wiersze
function is_file {
if [ ! -f "$1" ] ; then
echo "$1 nie jest poprawnym plikiem"
exit 2
fi
}
clean_file() {
is_file "$1"
BEFORE=$(wc -l "$1")
echo "Przed modyfikacją plik $1 miał $BEFORE wierszy."
sed -i.bak '/^\s*#/d;/^$/d' "$1"
AFTER=$(wc -l "$1")
echo "Po modyfikacji plik $1 ma $AFTER wierszy."
}
read -p "Podaj nazwę pliku: "
clean_file "$REPLY"
exit 1