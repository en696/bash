#!/bin/bash


CPU_CORES=$(grep -c name /proc/cpuinfo)
if (( CPU_CORES < 4 )) ; then
echo "Wymagany procesor z co najmniej czterema rdzeniami!"
exit 1
else
echo "Maszyna posiada $CPU_CORES corow"
fi